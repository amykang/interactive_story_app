package com.teamtreehouse.interactivestory.model;

import com.teamtreehouse.interactivestory.R;

/**
 * Created by amy on 8/20/17.
 */

public class Story {

    private Page[] pages;

    public Story() {
        pages = new Page[4];

        pages[0] = new Page(R.drawable.page0, R.string.page0,
                new Choice(R.string.page0_choice1, 1),
                new Choice(R.string.page0_choice2, 2));

        pages[1] = new Page(R.drawable.page1, R.string.page1,
                new Choice(R.string.page1_choice1, 2),
                new Choice(R.string.page1_choice2, 3));

        pages[2] = new Page(R.drawable.page2, R.string.page2,
                new Choice(R.string.page2_choice1, 3));

        pages[3] = new Page(R.drawable.page3, R.string.page3);
    }

    public Page getPage(int pageNumber) {
        if (pageNumber >= pages.length) {
            pageNumber = 0;
        }
        return pages[pageNumber];
    }
}
